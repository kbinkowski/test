import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;

/**
 * @author Kamil
 *
 */
public class Test {
	
	/**
	 * Source of data 
	 */
	BufferedReader stream;

	/**
	 * Initialization of data source. 
	 * @param name - path to file with data
	 * @throws UnsupportedEncodingException
	 * @throws FileNotFoundException
	 */
	Test(String name) throws UnsupportedEncodingException, FileNotFoundException{
		stream = new BufferedReader(new InputStreamReader(
				new FileInputStream(new File(name)), "UTF8"));
	}
	
	/**
	 * Closing the input stream.
	 * @throws IOException
	 */
	void clear() throws IOException{
		stream.close();
	}	
	
	/**
	 * Calculation the sum of the cost of all transaction
	 * @return BigDecimal
	 * @throws IOException
	 */
	BigDecimal calculate() throws IOException{
		BigDecimal amount = new BigDecimal(0.0);
		String line; 
		int pos1;
		int pos2;
		
		while( (line=stream.readLine()) != null ){
			pos1 = line.indexOf("@amount")+8;
			pos2 = line.indexOf("PLN");
			
			if( pos1==-1 || pos2==-1 )
				continue;
				
			amount = amount.add(new BigDecimal(line.substring(pos1, pos2).replaceAll(",", ".")));
		}
		
		return amount;
	}

	/**
	 * Test method
	 * @param args - path to file of data
	 */
	public static void main(String args[]){
		String name;
		
		if(args.length==0)
			name = "Plik_z_danymi.txt";
		else
			name = args[0];	

		try {
			Test t = new Test(name);
			System.out.println("The total amount: " + t.calculate().toString() + " PLN.");
			t.clear();
		} catch (FileNotFoundException e) {
			System.out.println("File not exists: "+name);
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			System.out.println("Problem in reading file. Bad format.");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Problem in reading file.");
			e.printStackTrace();
		} 
	}
}
